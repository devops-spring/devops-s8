#!/bin/zsh

# Подробные логи выполняемых команд
set -x

# Не продолжать выполнение команд при ошибке
set -e

# Сборка приложения при помощи кэширующего бэкенда

# Создание локального билдера с кэшем
# -----------------------------------
# docker buildx create --use --driver=docker-container
# > wonderful_sinoussi

# Или используем созданный
# ------------------------
# docker buildx use wonderful_sinoussi

# Сборка
docker compose -f docker-compose.build.yaml build

# Запуск приложения
docker compose -f docker-compose.yaml up
