#!/bin/zsh

# Подробные логи выполняемых команд
set -x

# Не продолжать выполнение команд при ошибке
set -e

BUILD_FILE_NAME='docker-compose.build.yaml'

# Сборка приложения при помощи кэширующего бэкенда

# Создание локального билдера с кэшем
# -----------------------------------
# docker buildx create --use --driver=docker-container
# > wonderful_sinoussi

# Или используем созданный
# ------------------------
# docker buildx use wonderful_sinoussi

# Смонтировать зависимости
./mount.zsh || true

INIT_PWD=$(pwd)

# Сборка
for composeFile in $(find . -type f -depth 2 -name ${BUILD_FILE_NAME}); do
    cd $(dirname ${composeFile})
    docker compose -f ${BUILD_FILE_NAME} build
    cd ${INIT_PWD}
done
