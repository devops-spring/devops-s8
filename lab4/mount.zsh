#!/bin/zsh
cd $(dirname $0)

if [[ ! -d app ]]; then
    mkdir app
    echo app > .gitignore
fi

bindfs -r ../3/app ./app
