CREATE TABLE keys (
    id BIGSERIAL PRIMARY KEY NOT NULL,
    key TEXT NOT NULL
);

INSERT INTO keys (key)
    VALUES ('First key');
