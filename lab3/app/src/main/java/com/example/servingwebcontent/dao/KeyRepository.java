package com.example.servingwebcontent.dao;

import com.example.servingwebcontent.model.KeyModel;
import org.springframework.data.repository.CrudRepository;

public interface KeyRepository extends CrudRepository<KeyModel, Long> {
}
