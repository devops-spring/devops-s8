# devops-s8

## Лабораторные работы по предмету DevOPS

- [x] Lab1: Gitlab
- [x] Lab2: Docker Запуск nginx
- [x] Lab3: Docker: докеризация приложения
- [x] Lab4: Docker: Мультистейджинг, сборка и запуск
- [x] Lab5: Docker: Мультистейджинг, различные уровни зависимостей
- [x] Lab6: Docker: Docker-compose и мультирепозиторий

